;; input.cljs
(ns shadow-cljs-fuckabout.input
  (:require [reagent.core :as r]))

(defonce input-state (r/atom {:left false :right false :up false :down false :W false :A false :S false :D false}))

(defn set-key-down [key]
  (swap! input-state assoc key true))

(defn set-key-up [key]
  (swap! input-state assoc key false))
