;; core.cljs
(ns shadow-cljs-fuckabout.core
  (:require [shadow-cljs-fuckabout.circle :as circle], reagent.core))

(defn clear-canvas [ctx]
  (.clearRect ctx 0 0 800 600))

(defn ^:export init []
  (let [ctx (circle/get-canvas-context)]
    (circle/start)
    (reagent.core/track!
     (fn []
       (clear-canvas ctx)
       (let [{:keys [x y]} @circle/app-state]
         (circle/draw ctx x y 20 "blue"))))))

(init)
