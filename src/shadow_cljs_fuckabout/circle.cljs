;; circle.cljs
(ns shadow-cljs-fuckabout.circle
  (:require [shadow-cljs-fuckabout.input :as input]
            [reagent.core :as r]))

(defonce app-state (r/atom {:x 50 :y 50 :radius 20 :color "blue" :speed 4}))

(defn get-canvas-context []
  (.getContext (.getElementById js/document "canvas") "2d"))

(defn move-circle []
  (let [{:keys [left right up down]} @input/input-state
        {:keys [speed]} @app-state]
    (swap! app-state update :x (fn [x]
                                 (cond
                                   left (- x speed)
                                   right (+ x speed)
                                   :else x)))
    (swap! app-state update :y (fn [y]
                                 (cond
                                   up (- y speed)
                                   down (+ y speed)
                                   :else y)))))

(defn draw [ctx x y radius color]
  (set! (.-fillStyle ctx) color)
  (.beginPath ctx)
  (.arc ctx x y radius 0 (* 2 Math/PI))
  (.fill ctx))

(defn start []
  (let [ctx (get-canvas-context)]
    (.addEventListener js/window "keydown" (fn [e]
                                             (let [key (.-key e)]
                                               (case key
                                                 "ArrowLeft" (input/set-key-down :left)
                                                 "ArrowRight" (input/set-key-down :right)
                                                 "ArrowUp" (input/set-key-down :up)
                                                 "ArrowDown" (input/set-key-down :down)
                                                 nil))))
    (.addEventListener js/window "keyup" (fn [e]
                                           (let [key (.-key e)]
                                             (case key
                                               "ArrowLeft" (input/set-key-up :left)
                                               "ArrowRight" (input/set-key-up :right)
                                               "ArrowUp" (input/set-key-up :up)
                                               "ArrowDown" (input/set-key-up :down)
                                               nil))))
    (js/setInterval move-circle 10)
    (r/track
     (fn []
       (let [{:keys [x y radius color]} @app-state]
         (draw ctx x y radius color))))))
